// 1.var и let - переменные которые могут перезаписаваться на пртяжении выполнения кода;
// const - переменная которая записывается один раз 
// 2. отличия в области видимости! var - за счет "всплытия" объявляется в начале скрипта или функции, тогда как let объявляется, тогда, когда до него дойдет очередь)

let userName;
let userAge;
let confAnswer;

do {
    userName = prompt('what is you name?');
} while (userName === ' ' || userName === null || userName === '' || userName == Number(userName));

do {
    userAge = parseInt(prompt('what is you age?'));
    console.log(userAge);
} while (isNaN(Number.parseFloat(userAge)) || userAge === null || userAge === 0);

if(userAge < 18 ) {
    alert("You are not allowed to visit this website");
} else if(userAge > 22) {
    alert('Welcome ' + userName);
} else {
      if (confirm('Are you sure you want to continue?')) {
       alert ('Welcome ' + userName);
   } else {
       alert ('You are not allowed to visit this website');
   }
}

;